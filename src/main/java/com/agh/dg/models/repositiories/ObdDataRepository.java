package com.agh.dg.models.repositiories;

import com.agh.dg.models.ObdDataModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.UUID;

/**
 * Created by Damian on 30.11.2016.
 */
@Transactional
@Repository
public interface ObdDataRepository extends CrudRepository<ObdDataModel, Long> {

    @Query(value = "select o from ObdDataModel o where o.profileId = ?1 and o.tripName = ?2 and o.latitude is not null and o.longitude is not null order by time asc")
    Set<ObdDataModel> findForMap(UUID profileId, String tripName);

    Page<ObdDataModel> findByProfileId(UUID profileId, Pageable pageable);

    Page<ObdDataModel> findByProfileIdAndTripName(UUID profileId, String tripName, Pageable pageable);

    @Query(value = "select o.tripName from ObdDataModel o where o.profileId = ?1 and o.tripName is not null group by o.tripName")
    Set<String> findTripNames(UUID profileId);

    @Query(value = "select MAX(o.time) from ObdDataModel o where o.profileId = ?1")
    Long getLastDataTimestamp(UUID profileId);

    @Query(value = "select o from ObdDataModel o where o.profileId = ?1 and time > ?2")
    Set<ObdDataModel> findAllFrom(UUID profileId, long from);

}
