package com.agh.dg.models.repositiories;


import com.agh.dg.models.AuthTokensModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Transactional
@Repository
public interface AuthTokensRepository extends CrudRepository<AuthTokensModel, Integer> {
    AuthTokensModel findFirstByToken(UUID token);
}
