package com.agh.dg.models.repositiories;

import com.agh.dg.models.ProfilesModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * Created by Damian on 30.11.2016.
 */
@Transactional
@Repository
public interface ProfilesRepository extends CrudRepository<ProfilesModel, UUID> {
}

