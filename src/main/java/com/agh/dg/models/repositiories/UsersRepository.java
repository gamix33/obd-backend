package com.agh.dg.models.repositiories;

import com.agh.dg.models.UsersModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@Repository
public interface UsersRepository extends CrudRepository<UsersModel, Integer> {

    UsersModel findFirstByEmailAndPassword(String email, String password);
}