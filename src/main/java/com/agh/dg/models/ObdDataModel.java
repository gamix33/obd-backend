package com.agh.dg.models;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

/**
 * Created by Damian on 30.11.2016.
 */

@Entity
@Table(name = "obd_data", uniqueConstraints={@UniqueConstraint(columnNames = {"profile_id" , "time"})})
public class ObdDataModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(columnDefinition = "DECIMAL(10,2)") private Double latitude;
    @Column(columnDefinition = "DECIMAL(10,2)") private Double longitude;
    @Column(columnDefinition = "DECIMAL(10,2)") private String tripName;
    @NotNull private long time;
    private Integer rpm;
    @Column(columnDefinition = "DECIMAL(10,2)") private Double speed;
    @Column(columnDefinition = "DECIMAL(10,2)") private Double acuVoltage;
    @Column(columnDefinition = "DECIMAL(10,2)") private Double airIntakeTemperature;
    @Column(columnDefinition = "DECIMAL(10,2)") private Double engineCoolantTemperature;
    @Column(columnDefinition = "DECIMAL(10,2)") private Double oilTemperature;
    @Column(columnDefinition = "DECIMAL(10,2)") private Double throttlePosition;
    @Column(columnDefinition = "DECIMAL(10,2)") private Double fuelLevel;
    @Column(columnDefinition = "DECIMAL(10,2)") private Double fuelConsumption;

    @NotNull
    @Column(name="profile_id", columnDefinition = "VARCHAR(36)")
    @Type(type="uuid-char")
    private UUID profileId;

    public ObdDataModel(){}

    public ObdDataModel(Double latitude, Double longitude, String tripName, long time, Integer rpm, Double speed, Double acuVoltage, Double airIntakeTemperature, Double engineCoolantTemperature, Double oilTemperature, Double throttlePosition, Double fuelLevel, Double fuelConsumption, UUID profileId) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.tripName = tripName;
        this.time = time;
        this.rpm = rpm;
        this.speed = speed;
        this.acuVoltage = acuVoltage;
        this.airIntakeTemperature = airIntakeTemperature;
        this.engineCoolantTemperature = engineCoolantTemperature;
        this.oilTemperature = oilTemperature;
        this.throttlePosition = throttlePosition;
        this.fuelLevel = fuelLevel;
        this.fuelConsumption = fuelConsumption;
        this.profileId = profileId;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Integer getRpm() {
        return rpm;
    }

    public void setRpm(Integer rpm) {
        this.rpm = rpm;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getAcuVoltage() {
        return acuVoltage;
    }

    public void setAcuVoltage(Double acuVoltage) {
        this.acuVoltage = acuVoltage;
    }

    public Double getAirIntakeTemperature() {
        return airIntakeTemperature;
    }

    public void setAirIntakeTemperature(Double airIntakeTemperature) {
        this.airIntakeTemperature = airIntakeTemperature;
    }

    public Double getEngineCoolantTemperature() {
        return engineCoolantTemperature;
    }

    public void setEngineCoolantTemperature(Double engineCoolantTemperature) {
        this.engineCoolantTemperature = engineCoolantTemperature;
    }

    public Double getOilTemperature() {
        return oilTemperature;
    }

    public void setOilTemperature(Double oilTemperature) {
        this.oilTemperature = oilTemperature;
    }

    public Double getThrottlePosition() {
        return throttlePosition;
    }

    public void setThrottlePosition(Double throttlePosition) {
        this.throttlePosition = throttlePosition;
    }

    public Double getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(Double fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public Double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(Double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public UUID getProfileId() {
        return profileId;
    }

    public void setProfileId(UUID profileId) {
        this.profileId = profileId;
    }
}
