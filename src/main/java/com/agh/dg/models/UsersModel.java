package com.agh.dg.models;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;


@Entity
@Table(name = "users", uniqueConstraints={@UniqueConstraint(columnNames={"email"})})
public class UsersModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull
    private String email;

    @NotNull
    private String password;

    @NotNull
    @Column(columnDefinition="DATETIME")
    private Date registered;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<ProfilesModel> profiles;

    public UsersModel() {
        this.registered = new Date();
    }

    public UsersModel(String email, String password) {
        this.email = email;
        this.password = password;
        this.registered = new Date();
    }

    @Transient
    public void encryptPassword(){
        this.password = Hashing.sha1().hashString(this.password, Charsets.UTF_8 ).toString();
    }

    @Transient
    public String getEncryptedPassword(){
        return Hashing.sha1().hashString(this.password, Charsets.UTF_8 ).toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public Date getRegistered() {
        return registered;
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }

    public Set<ProfilesModel> getProfiles() {
        return profiles;
    }

    public void setProfiles(Set<ProfilesModel> profiles) {
        this.profiles = profiles;
    }
}