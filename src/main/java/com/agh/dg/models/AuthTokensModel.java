package com.agh.dg.models;


import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "auth_tokens", uniqueConstraints = {@UniqueConstraint(name = "ut_u_token", columnNames = "token")})
public class AuthTokensModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private UsersModel user;

    @NotNull
    @Column(columnDefinition = "VARCHAR(36)")
    @Type(type="uuid-char")
    private UUID token;

    public AuthTokensModel(){}

    public AuthTokensModel(UsersModel user){
        this.user = user;
        this.token = UUID.randomUUID();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UsersModel getUser() {
        return user;
    }

    public void setUser(UsersModel user) {
        this.user = user;
    }

    public UUID getToken() {
        return token;
    }

    public void setToken(UUID token) {
        this.token = token;
    }
}
