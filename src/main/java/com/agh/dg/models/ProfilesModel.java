package com.agh.dg.models;

import com.agh.dg.models.enums.EngineType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Damian on 30.11.2016.
 */
@Entity
@Table(name = "profiles")
public class ProfilesModel {

    @Id
    @Column(columnDefinition = "VARCHAR(36)")
    @Type(type="uuid-char")
    private UUID id;

    @NotNull
    @Column(columnDefinition = "VARCHAR(100)")
    private String name;

    private Integer year;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "VARCHAR(20)")
    private EngineType type;

    private Integer capacity;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UsersModel user;

    public ProfilesModel() {
        this.id = UUID.randomUUID();
        this.name = "Pojazd osobowy";
    }

    public ProfilesModel(String name, Integer year, String type, Integer capacity) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.year = year;
        this.type = EngineType.valueOf(type);
        this.capacity = capacity;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public EngineType getType() {
        return type;
    }

    public void setType(EngineType type) {
        this.type = type;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public UsersModel getUser() {
        return user;
    }

    public void setUser(UsersModel user) {
        this.user = user;
    }

}
