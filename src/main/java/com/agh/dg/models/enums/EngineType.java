package com.agh.dg.models.enums;

/**
 * Created by Damian on 30.11.2016.
 */
public enum EngineType {
    benzine, diesel, gas, hybrid
}
