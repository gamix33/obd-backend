package com.agh.dg.rest;

import com.agh.dg.models.ProfilesModel;
import com.agh.dg.models.UsersModel;
import com.agh.dg.models.AuthTokensModel;
import com.agh.dg.models.repositiories.UsersRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

@RestController
@RequestMapping(value = "/users")
public class Users extends AuthController{

    @Autowired
    protected UsersRepository usersRepository;

    /**
     * Rejestracja
     * @param userModel
     * @return
     */
    @RequestMapping(value = "/register" , method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<HashMap<String, Object>> registerAction(@RequestBody UsersModel userModel){
        userModel.encryptPassword();

        try {
            usersRepository.save(userModel);
        } catch (Exception e) {
            throw new EmailUsedException();
        }

        HashMap<String, Object> respose = new HashMap<>();
        respose.put("id", userModel.getId());
        respose.put("email", userModel.getEmail());
        respose.put("token", generateToken(userModel));

        ProfilesModel profilesModel = new ProfilesModel();
        profilesModel.setUser(userModel);
        profilesRepository.save(profilesModel);

        ArrayList<UUID> profilesList = new ArrayList<>();
        profilesList.add(profilesModel.getId());
        respose.put("profiles", profilesList);

        return new ResponseEntity<>(respose, HttpStatus.OK);
    }

    /**
     * Logowanie
     * @param _userModel
     * @return
     */
    @RequestMapping(value = "/login" , method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<HashMap<String, Object>> loginAction(@RequestBody UsersModel _userModel){
        UsersModel userModel = usersRepository.findFirstByEmailAndPassword(_userModel.getEmail(), _userModel.getEncryptedPassword());

        if(userModel == null){
            throw new WrongEmailOrPassException();
        }

        HashMap<String, Object> respose = new HashMap<>();
        respose.put("id", userModel.getId());
        respose.put("email", userModel.getEmail());
        respose.put("token", generateToken(userModel));

        ArrayList<UUID> profiles = new ArrayList<>();
        for(ProfilesModel profilesModel : userModel.getProfiles()){
            profiles.add(profilesModel.getId());
        }
        respose.put("profiles", profiles);

        return new ResponseEntity<>(respose, HttpStatus.OK);
    }

    /**
     * Autoryzacja
     * @param token
     * @return
     */
    @RequestMapping(value = "/auth" , method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<HashMap<String, Object>> authAction(@RequestHeader(value="Authorization") UUID token){
        UsersModel userModel = auth(token);

        HashMap<String, Object> respose = new HashMap<>();
        respose.put("id", userModel.getId());
        respose.put("email", userModel.getEmail());
        respose.put("token", token);

        ArrayList<UUID> profiles = new ArrayList<>();
        for(ProfilesModel profilesModel : userModel.getProfiles()){
            profiles.add(profilesModel.getId());
        }
        respose.put("profiles", profiles);

        return new ResponseEntity<>(respose, HttpStatus.OK);
    }

    /**
     * Wylogowanie
     * @param token
     * @return
     */
    @RequestMapping(value = "/logout" , method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<Boolean> logoutAction(@RequestHeader(value="Authorization") UUID token){
        AuthTokensModel tokenModel = authTokensRepository.findFirstByToken(token);

        if(tokenModel == null){
            return new ResponseEntity<>(false, HttpStatus.OK);
        }

        authTokensRepository.delete(tokenModel);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @ResponseStatus(value=HttpStatus.METHOD_NOT_ALLOWED, reason="Email used")
    private class EmailUsedException extends RuntimeException {}

    @ResponseStatus(value=HttpStatus.UNAUTHORIZED, reason="Wrong email or password")
    private class WrongEmailOrPassException extends RuntimeException {}

    private UUID generateToken(UsersModel user){
        AuthTokensModel tokenModel = new AuthTokensModel(user);
        authTokensRepository.save(tokenModel);

        return tokenModel.getToken();
    }
}
