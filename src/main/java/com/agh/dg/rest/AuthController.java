package com.agh.dg.rest;

import com.agh.dg.models.AuthTokensModel;
import com.agh.dg.models.ProfilesModel;
import com.agh.dg.models.UsersModel;
import com.agh.dg.models.repositiories.AuthTokensRepository;
import com.agh.dg.models.repositiories.ProfilesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

/**
 * Created by Damian on 01.12.2016.
 */
public abstract class AuthController {

    @Autowired
    protected AuthTokensRepository authTokensRepository;

    @Autowired
    protected ProfilesRepository profilesRepository;

    @ResponseStatus(value= HttpStatus.FORBIDDEN, reason="Invalid token")
    protected class AuthException extends RuntimeException {}

    @ResponseStatus(value= HttpStatus.FORBIDDEN, reason="Profile not found")
    protected class ProfileNotFoundException extends RuntimeException {}

    protected UsersModel auth(UUID token){
        AuthTokensModel tokenModel = authTokensRepository.findFirstByToken(token);

        if(tokenModel == null){
            throw new AuthException();
        }

        return tokenModel.getUser();
    }

    protected ProfilesModel profile(UUID authToken, UUID profileId){
        UsersModel user = auth(authToken);

        ProfilesModel profilesModel = profilesRepository.findOne(profileId);

        if(profilesModel == null || !profilesModel.getUser().equals(user)){
            throw new ProfileNotFoundException();
        }

        return profilesModel;
    }

}
