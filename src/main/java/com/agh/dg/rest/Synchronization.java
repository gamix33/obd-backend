package com.agh.dg.rest;

import com.agh.dg.models.ObdDataModel;
import com.agh.dg.models.repositiories.ObdDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Created by Damian on 30.11.2016.
 */
@RestController
@RequestMapping(value = "/synchronization")
public class Synchronization extends AuthController {

    @Autowired
    ObdDataRepository obdDataRepository;

    /**
     * Zwraca timestamp najnowszego rekrodu zebranych danych
     * @param authToken
     * @return
     */
    @RequestMapping(value = "/{profile_id}/check" , method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<HashMap<String, Long>> getNewestRecordTime(
        @RequestHeader(value="Authorization") UUID authToken,
        @PathVariable("profile_id") UUID profileId
    ){
        profile(authToken, profileId);
        Long lastDataTimestamp = obdDataRepository.getLastDataTimestamp(profileId);
        if(lastDataTimestamp == null){
            lastDataTimestamp = 0L;
        }
        HashMap<String, Long> result = new HashMap<>();
        result.put("data", lastDataTimestamp);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Wysyła dane
     * @param authToken
     * @param records
     * @return
     */
    @RequestMapping(value = "/{profile_id}/upload" , method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<HashMap<String, Long>> uploadData(
        @RequestHeader(value="Authorization") UUID authToken,
        @PathVariable("profile_id") UUID profileId,
        @RequestBody HashSet<ObdDataModel> records
    ){
        profile(authToken, profileId);

        for(ObdDataModel record: records){
            record.setProfileId(profileId);
            obdDataRepository.save(record);
        }

        HashMap<String, Long> result = new HashMap<>();
        Long lastDataTimestamp = obdDataRepository.getLastDataTimestamp(profileId);
        result.put("data", lastDataTimestamp);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Pobiera dane
     * @param authToken
     * @param from
     * @return
     */
    @RequestMapping(value = "/{profile_id}/download/{from}" , method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Set<ObdDataModel>> downloadData(
        @RequestHeader(value="Authorization") UUID authToken,
        @PathVariable("profile_id") UUID profileId,
        @PathVariable("from") Long from
    ){
        profile(authToken, profileId);

        Set<ObdDataModel> result = obdDataRepository.findAllFrom(profileId, from);

        return new ResponseEntity(result, HttpStatus.OK);
    }
}
