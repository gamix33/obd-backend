package com.agh.dg.rest;

import com.agh.dg.models.ProfilesModel;
import com.agh.dg.models.UsersModel;
import com.agh.dg.models.repositiories.ProfilesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.UUID;

/**
 * Created by Damian on 01.12.2016.
 */
@RestController
@RequestMapping(value = "/profiles")
public class Profiles extends AuthController {

    @Autowired
    ProfilesRepository profilesRepository;

    /**
     * Dodawanie nowego profilu pojazdu
     * @param profile
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<ProfilesModel> addAction(
            @RequestHeader(value="Authorization") UUID authToken,
            @RequestBody ProfilesModel profile
    ){
        UsersModel user = auth(authToken);
        profile.setUser(user);
        profile = profilesRepository.save(profile);
        return new ResponseEntity<>(profile, HttpStatus.OK);
    }

    /**
     * Usunięcie profilu
     * @param profileId
     * @return
     */
    @RequestMapping( value = "/{profile_id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<Boolean> deleteAction(
            @RequestHeader(value="Authorization") UUID authToken,
            @PathVariable("profile_id") UUID profileId
    ){
        ProfilesModel profile = profile(authToken, profileId);
        profilesRepository.delete(profile);

        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    /**
     * Pobranie listy profili
     */
    @RequestMapping(method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<Set<ProfilesModel>> getProfilesListAction(@RequestHeader(value="Authorization") UUID authToken){
        UsersModel user = auth(authToken);
        Set<ProfilesModel> profilesList = user.getProfiles();

        return new ResponseEntity<>(profilesList, HttpStatus.OK);
    }

}
