package com.agh.dg.rest;

import com.agh.dg.models.ObdDataModel;
import com.agh.dg.models.repositiories.ObdDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Damian on 30.11.2016.
 */
@RestController
@RequestMapping(value = "/obd-data")
public class ObdData extends AuthController {

    @Autowired
    ObdDataRepository obdDataRepository;

    @RequestMapping(value = "/{profile_id}/trips" , method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Set<String>> getTripsList(
            @RequestHeader(value="Authorization") UUID authToken,
            @PathVariable("profile_id") UUID profileId
    ){
        profile(authToken, profileId);
        Set<String> result = obdDataRepository.findTripNames(profileId);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/{profile_id}/trips/{trip_name}" , method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Set<ObdDataModel>> getTripData(
        @RequestHeader(value="Authorization") UUID authToken,
        @PathVariable("profile_id") UUID profileId,
        @PathVariable("trip_name") String tripName
    ){
        try {
            tripName = java.net.URLDecoder.decode(tripName, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return new ResponseEntity<>((Set<ObdDataModel>) null, HttpStatus.BAD_REQUEST);
        }
        profile(authToken, profileId);
        Set<ObdDataModel> result = obdDataRepository.findForMap(profileId, tripName);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/{profile_id}/chart/{page}" , method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Page<ObdDataModel>> getChartData(
        @RequestHeader(value="Authorization") UUID authToken,
        @PathVariable("profile_id") UUID profileId,
        @PathVariable("page") int page
    ){
        profile(authToken, profileId);

        if(page < 0)  page = 0;
        int size = 25;
        Pageable pageable = new PageRequest(page, size, Sort.Direction.DESC, "time");
        Page<ObdDataModel> result = obdDataRepository.findByProfileId(profileId, pageable);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/{profile_id}/chart/{trip_name}/{page}" , method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Page<ObdDataModel>> getChartTripData(
        @RequestHeader(value="Authorization") UUID authToken,
        @PathVariable("profile_id") UUID profileId,
        @PathVariable("trip_name") String tripName,
        @PathVariable("page") int page
    ){
        profile(authToken, profileId);
        try {
            tripName = java.net.URLDecoder.decode(tripName, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return new ResponseEntity<>((Page<ObdDataModel>) null, HttpStatus.BAD_REQUEST);
        }

        if(page < 0)  page = 0;
        int size = 25;
        Pageable pageable = new PageRequest(page, size, Sort.Direction.ASC, "time");
        Page<ObdDataModel> result = obdDataRepository.findByProfileIdAndTripName(profileId, tripName, pageable);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
