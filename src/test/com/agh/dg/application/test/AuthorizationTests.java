package com.agh.dg.application.test;

import com.agh.dg.models.UsersModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.ResourceAccessException;

import java.util.HashMap;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthorizationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void success_LoginTest() throws Exception {
        String email = "damian@test.pl";
        String password = "test12345";

        UsersModel usersModel = new UsersModel(email, password);
        ResponseEntity<HashMap> responseEntity = restTemplate.postForEntity("/users/login", usersModel, HashMap.class);

        HashMap responseBody = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseBody.get("email"), email);
    }

    @Test
    public void error_LoginTest() throws Exception {
        String email = "damian@test.pl";
        String password = UUID.randomUUID().toString();

        UsersModel usersModel = new UsersModel(email, password);
        try {
            ResponseEntity responseEntity = restTemplate.postForEntity("/users/login", usersModel, HashMap.class);
        } catch(ResourceAccessException e){
            //test successful
        }
    }
}